package ru.edu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class Reader {
    private String line;

    public Reader (String string) {
        try {
            Path path = Path.of(string);
            readSource(path.toFile());
        }
        catch (Exception e) {
            readSource(string);
        }
    }
    public Reader (Path path) {
        readSource(path.toFile());
    }
    public Reader (File file) {
        readSource(file);
    }

    public void readSource(String text) {
        line = text;
    }

    public void readSource(File file) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file));) {
            StringBuilder sb = new StringBuilder();
            int stringCounter = 0;
            while (reader.ready()) {
                if(stringCounter > 0)
                    sb.append(" ");
                sb.append(reader.readLine());
                stringCounter++;
            }
            line = sb.toString();
        } catch (IOException e) {
            System.out.println("IO operation failed in TextStatistics method");
            e.printStackTrace();
        }
    }
    public String getLine () {
        return line;
    }
}
