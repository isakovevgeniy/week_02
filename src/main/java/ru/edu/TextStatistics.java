package ru.edu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TextStatistics {
    private long wordsCount;
    private long charsCount;
    private long spaceCount;
    private long charsCountWithoutSpaces;
    private long charsCountOnlyPunctuations;
    private ArrayList<Object[]> top10 = new ArrayList<>();


    public TextStatistics(
            long wordsCount,
            long charsCount,
            long spaceCount,
            long charsCountOnlyPunctuations,
            ArrayList<Object[]> top10) {
        this.wordsCount = wordsCount;
        this.charsCount = charsCount;
        this.spaceCount = spaceCount;
        this.charsCountOnlyPunctuations = charsCountOnlyPunctuations;
        charsCountWithoutSpaces = charsCount - spaceCount;
        this.top10 = top10;
    }

    public long getWordsCount() {
        return wordsCount;
    }

    public long getCharsCount() {
        return charsCount;
    }

    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    public List<String> getTopWords() {
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder("Text statistics: ").append("\n");
        info.append("words count=").append(wordsCount).append("\n");
        info.append("chars count=").append(charsCount).append("\n");
        info.append("chars count without spaces=").append(charsCountWithoutSpaces).append("\n");
        info.append("punctuations chars count=").append(charsCountOnlyPunctuations).append("\n");
        info.append("Top 10 words=").append("\n");
        for(Object[] obj: top10) {
            info.append("Word ").append((String)obj[0]).append(":").append((Integer)obj[1]).append("\n");
        }
        return info.toString();
    }
}
