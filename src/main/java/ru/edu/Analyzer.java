package ru.edu;

import java.util.*;

/**
 * Анализатор строки текста
 */
public class Analyzer {
    private Map<String, Integer> wordsCollection = new TreeMap<>();
    private ArrayList<Object[]> top10 = new ArrayList<>();

    private long wordsCount;
    private long charsCount;
    private long spaceCount;
    private long charsCountOnlyPunctuations;

    public long getWordsCount() {
        return wordsCount;
    }
    public long getCharsCount() {
        return charsCount;
    }
    public long getSpaceCount() {
        return spaceCount;
    }
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Анализ строки произведения.
     * @param line
     */
    public TextStatistics analyze(final String line) {
        setWordsCount(line);
        setCharsCount(line);
        setTop10();
        return getStatistic();
    }

    /**
     * подсчет слов
     * @param line
     */
    private void setWordsCount(final String line) {
        String[] words = line.toLowerCase().split("[\\p{P} ]");
        for (String word : words) {
            if (word.equals("[\\p{Punct} ]") || word.equals("")) continue;
            if (wordsCollection.containsKey(word))
                wordsCollection.put(word, wordsCollection.get(word) + 1);
            else
                wordsCollection.put(word, 1);
            wordsCount++;
        }
    }

    /**
     *  Подсчет символов
     * @param line
     */
    private void setCharsCount(final String line) {
        for (char ch : line.toCharArray()) {
            charsCount++;
            String str = String.valueOf(ch);
            if (str.matches("\\p{P}")) {
                charsCountOnlyPunctuations++;
            }
            if (ch == ' ')
                spaceCount++;
        }
    }

    /**
     * выборка 10 самых повторяющихся слов
     */
    void setTop10() {
        List<Map.Entry<String, Integer>> entry = new ArrayList(wordsCollection.entrySet());
        Collections.sort(entry, Comparator.comparing(Map.Entry::getValue));
        int tmp = entry.get(entry.size()-1).getValue();
        for (int i = 0, j = 0; i < 10; j++) {
            if (j == entry.size()) break;
            int index = entry.size()-1-j;
            String word = entry.get(index).getKey();
            Integer wordCount = entry.get(index).getValue();
            if (wordCount < tmp) {
                tmp = wordCount;
                i++;
            }
            Object[] element = {word, wordCount};
            top10.add(element);
        }
    }

    public TextStatistics getStatistic() {
        return new TextStatistics(wordsCount, charsCount, spaceCount, charsCountOnlyPunctuations, top10);
    }
}
