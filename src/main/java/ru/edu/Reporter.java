package ru.edu;

import java.io.*;

public class Reporter {

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    public void getInfo(TextStatistics statistics) {
        System.out.println(statistics);
    }

    public void getInfoFromFile(String fileName) {
        File statistic = new File(fileName);
        try(BufferedReader reader = new BufferedReader(new FileReader(statistic)); ) {
            while (reader.ready())
                System.out.println(reader.readLine());
        } catch (IOException e) {
            System.out.println("IO operation failed in report method");
            e.printStackTrace();
        }
    }

    public void setInfoToFile(TextStatistics statistics, String fileName) {
        File statistic = new File(fileName);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(statistic)); ) {
            writer.write(statistics.toString());
        } catch (IOException e) {
            System.out.println("IO operation failed in report method");
            e.printStackTrace();
        }
    }
}
