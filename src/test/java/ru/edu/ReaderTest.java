package ru.edu;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;

public class ReaderTest {
    String text = "123456789 ?,";
    String textFilePath = "./src/test/resources/input_text.dat";
    Path path = Path.of(textFilePath);
    File file = path.toFile();

    Reader textReader;
    Reader textToPathReader;
    Reader pathReader;
    Reader fileReader;

    @Test
    public void textReaderTest() {
        textReader = new Reader(text);
        Assert.assertEquals("123456789 ?,",textReader.getLine());
    }
    @Test
    public void textToPathReaderTest() {
        textToPathReader = new Reader(textFilePath);
        Assert.assertEquals("123456789 ?,",textToPathReader.getLine());
    }
    @Test
    public void pathReaderTest() {
        pathReader = new Reader(path);
        Assert.assertEquals("123456789 ?,",pathReader.getLine());
    }
    @Test
    public void fileReaderTest() {
        fileReader = new Reader(file);
        Assert.assertEquals("123456789 ?,",fileReader.getLine());
    }
}