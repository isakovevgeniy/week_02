package ru.edu;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AnalyzerTest {
    private final String text = "123456789 456 ?,";

    private final long WORDS_COUNT = 2;
    private final long CHARS_COUNT = 16;
    private final long SPASE_COUNT = 2;
    private final long CHARS_COUNT_WITHOUT_SPASES = 14;
    private final long CHARS_COUNT_ONLY_PUNCTUATIONS = 2;

    Analyzer analyzer;

    @Before
    public void initialize() {
        analyzer = new Analyzer();
        analyzer.analyze(text);
    }

    @Test
    public void analyzeTest() {
        Assert.assertEquals(WORDS_COUNT, analyzer.getWordsCount());
        Assert.assertEquals(CHARS_COUNT, analyzer.getCharsCount());
        Assert.assertEquals(SPASE_COUNT, analyzer.getSpaceCount());
        Assert.assertEquals(CHARS_COUNT_ONLY_PUNCTUATIONS, analyzer.getCharsCountOnlyPunctuations());
    }

    @Test
    public void getStatisticTest() {
        TextStatistics textStatistics = analyzer.getStatistic();
        Assert.assertEquals(WORDS_COUNT, textStatistics.getWordsCount());
        Assert.assertEquals(CHARS_COUNT, textStatistics.getCharsCount());
        Assert.assertEquals(CHARS_COUNT_WITHOUT_SPASES, textStatistics.getCharsCountWithoutSpaces());
        Assert.assertEquals(CHARS_COUNT_ONLY_PUNCTUATIONS, textStatistics.getCharsCountOnlyPunctuations());
        Assert.assertNotNull(textStatistics.getTopWords());
    }
}