package ru.edu;

import org.junit.Test;
import java.util.ArrayList;

public class ReporterTest {

    static String statisticFilePath = "./src/test/resources/statistic.txt";
    static ArrayList<Object[]> top10Test = new ArrayList<>();

    static {
        Object[] first = {"word1", 12};
        Object[] second = {"word2", 18};
        top10Test.add(first);
        top10Test.add(second);
    }

    TextStatistics textStatistics = new TextStatistics(1, 12, 1, 11, top10Test);
    Reporter reporter = new Reporter();


    @Test
    public void reportTest() {
        reporter.getInfo(textStatistics);
    }

    @Test
    public void reportFromFileTest() {
        reporter.getInfoFromFile("./src/test/resources/statistic.txt");
    }

    @Test
    public void setInfoToFileTest() {
        reporter.setInfoToFile(textStatistics, "./src/test/resources/statistic_2.txt");
    }
}
