package ru.edu;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AnalyzerIntegaratedTest {
    private static final String dstStatisticFileName = "./src/test/resources/statisticsForIntegratedTest.data";
    private static final String srcStatisticFileName = "./src/test/resources/input_text.txt";
    @Test
    public void initialize() {
        Reader reader = new Reader(srcStatisticFileName);
        Analyzer analyzer = new Analyzer();

        new Reporter().setInfoToFile(analyzer.analyze(reader.getLine()), dstStatisticFileName);
    }

}